# ln-linear regression

Modèle « ln-linéaire généralisé », outil développé pour le traitement des données du suivi _in situ_ des installations d’ANC (Boutin et al., 2017) en collaboration avec Yves Le Gat (INRAE Nouvelle-Aquitaine Bordeaux)

Citation : FALIPOU E., LAOUAR I., BOUTIN C., LE GAT Y. (2021). Scripts du modèle ln-linéaire généralisé avec censure à gauche
